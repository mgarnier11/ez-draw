# Ez-draw

Chrome extension to draw beautifull images in a garticphone game

## Scripts

- `dev` starts webpack dev server with wtach enabled
- `build` build the extension in the `dist` folder

## How to use it ?

You need nodejs > 10

- run `npm install` to install required dependencies
- run `npm run build` or `npm run dev` to build the extension
- go to [chrome://extensions](chrome://extensions) and activate developper mode
- click on `load unpackaged extension` and select the dist folder you just built
- start a Garticphone game and hf !

## Project history

### The framework

Since I have no talent for drawing, I decided to make a bot to draw anything for me.

The 1st working version used libraries like [robotjs](https://www.npmjs.com/package/robotjs) and [iohook](https://www.npmjs.com/package/iohook) to take control over the mouse and move it programmatically to draw. It had a few downsides : I couldnt use my computer during the drawing, it was very slow and it only worked with 1 screen size.

I was not satisfied of how it worked so I then digged down on how garticphone was working, and I found it was possible to send direct MouseEvents to the drawing canvas. So, I used [electron](https://www.npmjs.com/package/electron) to render a controlable version of garticphone. It worked really well and now I can use my mouse during the drawing. The speed greatly increased but It was still to long for a garticphone game (a 1.5 hour drawing can now be done in 10 min)

The electron version was fun to watch but it appeared to have some problems when working on other screens than mine. I then decided to make a chrome extension to directly take control of the browser. Thanks to this, I can now send multiple MouseEvents during a single event loop (how js works : [fireship](https://www.youtube.com/watch?v=FSs_JYwnAdI)). Sending multiple MouseEvents dragged the drawing time from 10 min to 1 min for the same drawing

### The algorithm

There has been multiple version of the algorithm.

The 1st version was very simple, it was only able to draw points. It split the incoming image into multiple matrices of pixels (the size of these matrices depend on the drawing quality asked) and summs all of the pixels in the matrices into one pixel.

We now have a smaller matrix of pixels, then, for every pixel in the matrix we get the closest color of the garticphone allowed colors. We can now draw every pixel one by one as points

_1st version with a drawing quality of 5 (2min 30sec drawing)_
![1st version drawing quality of 5](./public/v1-q5.png?raw=true "v1 quality 5")

As you can see it is not very good and it was very slow.

The 2nd version is a small improvement of the 1st one. It takes the resulting matrix of the 1st version and sort it color by color. We now have multiple matrixes of pixels and we can draw every adjacent pixels of the same in a single line. We can now draw color by color, without losing the time to change color on every pixel.

_2nd version with a drawing quality of 3 (5 min drawing)_
![2nd version drawing quality of 3](./public/v2-q3.png?raw=true "v2 quality 3")

_2nd version with a drawing quality of 7 (30 sec drawing)_
![2nd version drawing quality of 7](./public/v2-q7.png?raw=true "v2 quality 7")

The 2nd version way faster and cleaner than the 1st but we still have some unwanted white points in the drawin. They are caused by the fact we draw using the "pen" tool of garticphone.

The 3rd is like the 2nd but designed to use the "rect" (or box) tool provided by garticphone. It is very precise and we can draw a box as small as 1 by 1 pixel.

_3rd version with a drawing quality of 7 (45 sec drawing)_
![3rd version drawing quality of 7](./public/v3-q7.png?raw=true "v3 quality 7")

_3rd version with a drawing quality of 1 (45min drawing)_
![3rd version drawing quality of 1](./public/v3-q1.png?raw=true "v3 quality 1")

The 3rd is nice and precise but it is very slow on a smaller qualities.

I needed to rehink the algorithm from the start. Instead of using lines of pixels I needed to get areas (boxes) of pixels. For example this matrix :

```
1 1 1 0 0 0 0 1 1 1
1 1 0 0 0 0 0 0 1 1
1 0 0 0 0 0 0 0 0 1
0 0 1 1 1 0 0 0 0 0
0 0 1 1 1 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
1 1 0 0 0 0 0 0 0 0
1 1 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 1 1
0 0 0 0 0 0 0 0 1 1
```

With the 3rd version we had 14 boxes to draw

```
1 3 5 0 0 0 0 10 11 13
1 3 0 0 0 0 0 0  11 13
1 0 0 0 0 0 0 0  0  13
0 0 6 7 8 0 0 0  0  0
0 0 6 7 8 0 0 0  0  0
0 0 0 0 0 0 0 0  0  0
2 4 0 0 0 0 0 0  0  0
2 4 0 0 0 0 9 0  0  0
0 0 0 0 0 0 0 0  12 14
0 0 0 0 0 0 0 0  12 14
```

With the 4th version we will have only 10 boxes to draw

```
1 3 4 0 0 0 0 7 8 10
1 3 0 0 0 0 0 0 8 10
1 0 0 0 0 0 0 0 0 10
0 0 5 5 5 0 0 0 0 0
0 0 5 5 5 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
2 2 0 0 0 0 0 0 0 0
2 2 0 0 0 0 6 0 0 0
0 0 0 0 0 0 0 0 9 9
0 0 0 0 0 0 0 0 9 9
```

I have also improved color accuracy by adding a list of 256 available colors.
Using electron or chrome extension, I can choose any rgb color

_4th version with a drawing quality of 1 (10min drawing)_
![4th version drawing quality of 1](./public/v4-q1.png?raw=true "v4 quality 1")

The 4th version is precise and fast, but not fast enough for a garticphone game.

In the 5th version, I used a simple loop to loop through 20 boxes before waiting 0ms to let javascript catch up all events.

_5th version with a drawing quality of 1 (1min 30sec drawing)_

![5th version demo drawing quality of 1](public/v5-q1.mp4)

[Image source](https://fr.freepik.com/vecteurs-premium/tigre-couche-isole-style-cartoon-illustration-zoologie-educative-image-livre-coloriage_6768311.htm)
