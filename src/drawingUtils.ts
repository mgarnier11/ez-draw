import * as Jimp from "jimp/browser/lib/jimp";

const { rgb2lab, lab2rgb, deltaE } = require("rgb-lab");

import { Box, Draw, ColorRGB, DrawType, Pixel, ColorHSL, Colors, Position } from "./interfaces";
import { Utils } from "./utils";

import garticColorFile from "./garticColors.json";
import colors8bitFile from "./256colors.json";
import colors5bitFile from "./64colors.json";
import lolsketchColorFile from "./lolsketch.json";

const garticColors: ColorRGB[] = garticColorFile.colors.map((c) => c.color);

const garticColorsLab: number[][] = garticColors.map((c) => rgb2lab([c.r, c.g, c.b]));

const lolsketchColors: ColorRGB[] = lolsketchColorFile.map((o) => o.color);

const colors8bit: ColorRGB[] = colors8bitFile.map((c) => c.rgb);

const colors8bitLab: number[][] = colors8bit.map((c) => rgb2lab([c.r, c.g, c.b]));

const colors5bit: ColorRGB[] = colors5bitFile.map((c) => c);

const colors5bitLab: number[][] = colors5bit.map((c) => rgb2lab([c.r, c.g, c.b]));

const colors12bit: ColorRGB[] = (() => {
  const colors: ColorRGB[] = [];

  for (let r = 1; r <= 256; r += 17) {
    for (let g = 1; g <= 256; g += 17) {
      for (let b = 1; b <= 256; b += 17) {
        colors.push({ r: r - 1, g: g - 1, b: b - 1 });
      }
    }
  }

  return colors;
})();

// const colors15bit: ColorRGB[] = (() => {
//   const colors: ColorRGB[] = [];

//   for (let r = -1; r <= 256; r += 8.25) {
//     for (let g = -1; g <= 256; g += 8.25) {
//       for (let b = -1; b <= 256; b += 8.25) {
//         colors.push({ r: Math.floor(r + 1), g: Math.floor(g + 1), b: Math.floor(b + 1) });
//       }
//     }
//   }

//   return colors;
// })();

export class DrawingUtils {
  static garticColors: ColorRGB[] = garticColors;
  static garticColorsLab: number[][] = garticColorsLab;
  static lolsketchColors: ColorRGB[] = lolsketchColors;
  static colors8bit: ColorRGB[] = colors8bit;
  static colors8bitLab: number[][] = colors8bitLab;
  static colors5bit: ColorRGB[] = colors5bit;
  static colors5bitLab: number[][] = colors5bitLab;
  static colors12bit: ColorRGB[] = colors12bit;
  // static colors15bit: ColorRGB[] = colors15bit;

  static async getPixelsMatrix(
    imageUrl: string,
    targetBox: Box,
    drawingQuality: number,
    colors: Colors
  ): Promise<Pixel[][]> {
    console.log(imageUrl, targetBox, drawingQuality, colors);

    const image = await Jimp.read(imageUrl).then((img) => img.resize(targetBox.width, targetBox.height));

    const pixels: Pixel[][] = [[]];

    for (let x = 0; x < targetBox.width / drawingQuality; x++) {
      pixels.push([]);

      // console.log(pixels.length);

      const xPos = x * drawingQuality;

      for (let y = 0; y < targetBox.height / drawingQuality; y++) {
        const yPos = y * drawingQuality;

        const pixelList: ColorRGB[] = [];
        // const pixelList: ColorHSL[] = [];

        for (let i = 0; i < drawingQuality; i++) {
          for (let j = 0; j < drawingQuality; j++) {
            const rgba = Jimp.intToRGBA(await image.getPixelColor(xPos + i, yPos + j));

            // pixelList.push(Utils.rgbToHsl({ r: rgba.r, g: rgba.g, b: rgba.b }));
            pixelList.push({ r: rgba.r, g: rgba.g, b: rgba.b });
          }
        }

        let color: ColorRGB;

        //TODO : use HSL instead of RGB
        //TODO fix closest color

        if (colors === Colors.Gartic) {
          color = Utils.closestColorv1(DrawingUtils.garticColors, Utils.averageColorv2(pixelList));
        } else if (colors === Colors.Bits8) {
          color = Utils.closestColorv1(DrawingUtils.colors8bit, Utils.averageColorv2(pixelList));
        } else if (colors === Colors.Bits5) {
          color = Utils.closestColorv1(DrawingUtils.colors5bit, Utils.averageColorv2(pixelList));
        } else if (colors === Colors.Bits12) {
          color = Utils.closestColorv1(DrawingUtils.colors12bit, Utils.averageColorv2(pixelList));
          // } else if (colors === Colors.Bits15) {
          //   color = Utils.closestColorv1(DrawingUtils.colors15bit, Utils.averageColorv2(pixelList));
        } else if (colors === Colors.True) {
          color = Utils.averageColorv2(pixelList);
        } else if (colors === Colors.Lolsketch) {
          color = Utils.closestColorv1(DrawingUtils.lolsketchColors, Utils.averageColorv2(pixelList));
        } else {
          color = { r: 0, g: 0, b: 0 };
        }

        pixels[x].push({
          color,
          position: { x, y },
        });

        // console.log(pixels[x].length);
      }
    }

    return pixels;
  }

  static getPointsToDraw(pixels: Pixel[][]): Draw[] {
    const pointsToDraw: Draw[] = [];

    for (let x = 0; x < pixels.length; x++) {
      for (let y = 0; y < pixels[x].length; y++) {
        const pixel = pixels[x][y];
        pointsToDraw.push({
          color: pixel.color,
          type: DrawType.Point,
          startPos: pixel.position,
          endPos: pixel.position,
        });
      }
    }

    return pointsToDraw;
  }

  static getLinesToDraw(pixels: Pixel[][]): Draw[] {
    const linesToDraw: Draw[] = [];

    const imageColors: ColorRGB[] = [
      ...new Set(Array.prototype.concat(...pixels.map((pixelLine) => pixelLine.map((pixel) => pixel.color)))),
    ].filter((c) => !Utils.colorsEquals(c, { r: 255, g: 255, b: 255 }));

    for (const color of imageColors) {
      // crée une nouvelle matrice avec uniquement les pixel de la couleur
      const thisColorPixels = pixels.map((pixelLine) =>
        pixelLine.map((c) => (Utils.colorsEquals(c.color, color) ? c : undefined))
      );

      for (let x = 0; x < thisColorPixels.length; x++) {
        const pixelLine = thisColorPixels[x];

        let startPos: Position | undefined;
        let endPos: Position | undefined;

        for (let y = 0; y <= pixelLine.length; y++) {
          const pixel = pixelLine[y];
          const nextPixel = pixelLine[y + 1];

          if (pixel !== undefined) {
            if (startPos === undefined) {
              startPos = {
                x: pixel.position.x,
                y: pixel.position.y,
              };
            }

            if (nextPixel === undefined) {
              endPos = {
                x: pixel.position.x,
                y: pixel.position.y,
              };
            }
          }

          if (startPos && endPos) {
            linesToDraw.push({
              color: color,
              startPos: startPos,
              endPos: endPos,
              type: DrawType.Line,
            });

            startPos = undefined;
            endPos = undefined;
          }
        }
      }
    }

    return linesToDraw;
  }

  static getBoxesToDraw(pixels: Pixel[][]): Draw[] {
    const boxesToDraw: Draw[] = [];

    // récupere les différentes couleurs de l'image tout en enlevant le blanc
    const imageColors: ColorRGB[] = [
      ...new Set(Array.prototype.concat(...pixels.map((pixelLine) => pixelLine.map((pixel) => pixel.color)))),
    ].filter((c) => !Utils.colorsEquals(c, { r: 255, g: 255, b: 255 }));

    console.log(imageColors);

    for (const color of imageColors) {
      // crée une nouvelle matrice avec uniquement les pixel de la couleur
      const thisColorPixels = pixels.map((pixelLine) =>
        pixelLine.map((c) => (Utils.colorsEquals(c.color, color) ? c : undefined))
      );

      //on initialize la mtrice de sortie
      const pixelMatrices: Pixel[][] = [[]];

      // pour chaque ligne de la matrice de pixels de cette couleur
      for (let x = 0; x < thisColorPixels.length; x++) {
        // récupere la ligne de pixels
        const pixelLine = thisColorPixels[x];

        // initialise une liste de séquence de pixels
        const pixelSequenceList: Pixel[][] = [[]];

        // pour chacun des pixels dans la ligne
        for (let i = 0; i < pixelLine.length; i++) {
          const pixel = pixelLine[i];

          // si le pixel est !== undefined, c'est qu'il a une couleur de définie
          // (normalement c'est la meme que tous les autres pixels présents dans la ligne)
          if (pixel !== undefined) {
            // récupere la derniere séquence de pixel
            const lastPixelSequence = pixelSequenceList[pixelSequenceList.length - 1];

            // récupere le dernier pixel de la séquence
            const lastPixel = lastPixelSequence[lastPixelSequence.length - 1];

            // s'il est défini ET qu'il est juste a coté du pixel actuel
            if (lastPixel && lastPixel.position.y + 1 === pixel.position.y) {
              // on ajoute le pixel actuel à la séquence
              lastPixelSequence.push(pixel);
            } else if (lastPixel === undefined) {
              // si le dernier pixel de la séquence n'est pas défini (la séquence est vide)
              // on ajoute le pixel actuel à la séquence
              lastPixelSequence.push(pixel);
            } else {
              // sinon on crée une nouvelle séquence de pixel avec le pixel actuel comme premier élément
              pixelSequenceList.push([pixel]);
            }
          }
        }

        // pour toutes les séquences de pixels non vide trouvées précédement
        for (const pixelSequence of pixelSequenceList) {
          if (pixelSequence.length > 0) {
            // on récupere la valeur x + 1 du premier pixel de la séquence
            let j = pixelSequence[0].position.x + 1;

            const computeNextPixelLines = (p: number) => {
              // on récupere la ligne de pixel suivant la derniere ligne vérifiée
              let nextPixelLine = thisColorPixels[p];

              // s'il y en a une
              if (nextPixelLine) {
                // on récupere les pixels situés sur la meme position y que ceux de la séquence actuelle
                // (on récupere la séquence de la prochaine ligne)
                const nextLineSequence = pixelSequence
                  .map((p) => nextPixelLine[p.position.y])
                  .filter((p) => p !== undefined) as Pixel[];

                // si la taille de la prochaine séquence et de la séquence actuelle sont identiques
                if (nextLineSequence.length === pixelSequence.length) {
                  // pour chque pixel de la prochaine séquence
                  for (const pixel of nextLineSequence) {
                    // on les enleve de la matrice de pixels de cette couleur, afin qu'ils ne
                    // soient pas traités lors de la prochaine itération
                    thisColorPixels[pixel.position.x][pixel.position.y] = undefined;

                    // on ajoute ensuite ces pixels à la derniere ligne de la matrice de sortie
                    pixelMatrices[pixelMatrices.length - 1].push(pixel);
                  }

                  //puis on passe à la ligne suivante
                  computeNextPixelLines(p + 1);
                } else {
                  // si la taille de la prochaine séquence et de la séquence actuelle sont différentes
                  // on ajoute tous les pixels de la séquence à la matrice de sortie
                  pixelMatrices[pixelMatrices.length - 1].unshift(...pixelSequence);

                  // puis on enleve tous les pixels de la matrice afin qui'ls ne
                  // soient pas traités lors de la prochaien itération
                  for (const pixel of pixelSequence) {
                    thisColorPixels[pixel.position.x][pixel.position.y] = undefined;
                  }
                }
              } else {
                // s'il n'y a pas de prochaine ligne
                // on ajoute la séquence à la matrice de sortie
                pixelMatrices[pixelMatrices.length - 1].unshift(...pixelSequence);

                // puis on enleve tous les pixels de la matrice afin qui'ls ne
                // soient pas traités lors de la prochaien itération
                for (const pixel of pixelSequence) {
                  thisColorPixels[pixel.position.x][pixel.position.y] = undefined;
                }
              }
            };

            // on la ligne suivant la ligne de la séquence actuelle
            computeNextPixelLines(j);

            // on ajoute ensuite un nouvelle liste vide pour la prochaine itération
            pixelMatrices.push([]);
          }
        }
      }

      // on enleve la liste vide ajoutée qui sera ajoutée à la derniere itération
      pixelMatrices.pop();

      // pour chacune des différentes listes dans la matrice
      for (const pixelList of pixelMatrices) {
        if (pixelList.length > 0) {
          // si la liste contient des pixels, on initalise une nouvelle boite avec la couleur définie
          const newBox: Draw = {
            type: DrawType.Box,
            color,
            startPos: { x: Infinity, y: Infinity },
            endPos: { x: -Infinity, y: -Infinity },
          };

          for (const pixel of pixelList) {
            // on regarde ensuite si les coordonnées de chacun des pixels sont plus a l'extreme que ceux dans la boite
            if (pixel.position.x < newBox.startPos.x) newBox.startPos.x = pixel.position.x;
            if (pixel.position.y < newBox.startPos.y) newBox.startPos.y = pixel.position.y;
            if (pixel.position.x > newBox.endPos.x) newBox.endPos.x = pixel.position.x;
            if (pixel.position.y > newBox.endPos.y) newBox.endPos.y = pixel.position.y;
          }

          boxesToDraw.push(newBox);
        }
      }
    }

    return boxesToDraw;
  }

  public static getLolsketchColorName(color: ColorRGB): string {
    const v = lolsketchColorFile.find((c) => c.color.r === color.r && c.color.g === color.g && c.color.b === color.b);

    if (v) return v.name;
    else return "";
  }
}
