export enum DrawType {
  Box,
  Line,
  Point,
}

export interface Box {
  x: number;
  y: number;
  width: number;
  height: number;
}

export interface Size {
  width: number;
  height: number;
}

export interface Position {
  x: number;
  y: number;
}

export interface ColorRGB {
  r: number;
  g: number;
  b: number;
}

export interface ColorHSL {
  h: number;
  s: number;
  l: number;
}

export interface Pixel {
  color: ColorRGB;
  position: Position;
}

export interface Draw {
  color: ColorRGB;
  type: DrawType;
  startPos: Position;
  endPos: Position;
}

export enum AppState {
  NotInGame = "Not in game",
  Clicking = "Waiting user to click",
  Waiting = "Waiting to start",
  Processing = "Processing",
  Finished = "Finished drawing",
  Paused = "Paused",
  Running = "Running",
}

export enum Colors {
  Gartic,
  Lolsketch,
  Bits5,
  Bits8,
  Bits12,
  // Bits15,
  True,
}

export enum SupportedSites {
  Garticphone = "garticphone.com",
  Lolsketch = "lolsketch.com",
}

export enum DrawingQualities {
  VerySmall,
  Small,
  Medium,
  Big,
}

export class Multisite {
  public static getActualSite(): SupportedSites | undefined {
    switch (window.location.hostname) {
      case SupportedSites.Garticphone:
        return SupportedSites.Garticphone;
      case SupportedSites.Lolsketch:
        return SupportedSites.Lolsketch;
    }
  }

  public static getRootGameElementTag(): string {
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        return "#content";
      case SupportedSites.Lolsketch:
        return "#root";
      default:
        return "";
    }
  }

  public static getDrawingContainerTag(): string {
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        return ".drawingContainer";
      case SupportedSites.Lolsketch:
        return ".canvas canvas";
      default:
        return "";
    }
  }

  public static getDefaultColors(): Colors {
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        return Colors.Gartic;
      case SupportedSites.Lolsketch:
        return Colors.Lolsketch;
      default:
        return Colors.Bits5;
    }
  }

  public static getQualityNb(q: DrawingQualities): number {
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        return Multisite.getQualityNbGartic(q);
      case SupportedSites.Lolsketch:
        return Multisite.getQualityNbLolsketch(q);
      default:
        return 0;
    }
  }

  private static getQualityNbGartic(q: DrawingQualities): number {
    switch (q) {
      case DrawingQualities.VerySmall:
        return 1;
      case DrawingQualities.Small:
        return 4;
      case DrawingQualities.Medium:
        return 7;
      case DrawingQualities.Big:
        return 10;
    }
  }

  private static getQualityNbLolsketch(q: DrawingQualities): number {
    switch (q) {
      case DrawingQualities.Small:
        return 1;
      case DrawingQualities.Medium:
        return 5;
      case DrawingQualities.Big:
        return 10;
      default:
        return 0;
    }
  }
}
