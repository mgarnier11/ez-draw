import { DrawingUtils } from "../drawingUtils";
import { Draw, DrawType } from "../interfaces";

const version = "1.3";

chrome.runtime.onMessage.addListener(async (message, sender) => {
  switch (message.command) {
    case "get-draws":
      const pixels = await DrawingUtils.getPixelsMatrix(
        message.imgUrl,
        message.targetBox,
        message.drawingQuality,
        message.colors
      );
      let draws: Draw[] = [];

      switch (message.drawType) {
        case DrawType.Box:
          draws = DrawingUtils.getBoxesToDraw(pixels);
          break;
        case DrawType.Line:
          draws = DrawingUtils.getLinesToDraw(pixels);
          break;
        case DrawType.Point:
          draws = DrawingUtils.getPointsToDraw(pixels);
          break;
        default:
          console.error("Unknown drawtype");
          break;
      }

      chrome.tabs.sendMessage(sender.tab?.id!, {
        command: "draws",
        draws: draws,
      });
      break;
    case "mouse-down-d":
      chrome.debugger.sendCommand({ tabId: sender.tab?.id, targetId: "test_patate" }, "Input.dispatchMouseEvent", {
        type: "mousePressed",
        x: message.clientX,
        y: message.clientY,
        button: "left",
        // clickCount: 1,
      });
      break;
    case "mouse-move-d":
      chrome.debugger.sendCommand({ tabId: sender.tab?.id }, "Input.dispatchMouseEvent", {
        type: "mouseMoved",
        x: message.clientX,
        y: message.clientY,
        buttons: 5,
      });
      break;
    case "mouse-up-d":
      chrome.debugger.sendCommand({ tabId: sender.tab?.id }, "Input.dispatchMouseEvent", {
        type: "mouseReleased",
        x: message.clientX,
        y: message.clientY,
        button: "left",
        // clickCount: 1,
      });
      break;

    case "get-draw-infos":
      break;
    case "attach-debugger":
      console.log("attach-debugger command received");
      chrome.tabs.query({ active: true, currentWindow: true }, (tabs: chrome.tabs.Tab[]) => {
        if (tabs.length > 0) {
          console.log("attach debugger");
          chrome.debugger.attach({ tabId: tabs[0].id! }, version, () => {
            console.log("debugger shoudl be attached");
          });
        }
      });
      break;
    default:
      console.log("unknow command : ", message);

      break;
  }
});
