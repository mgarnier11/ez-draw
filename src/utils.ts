const { rgb2lab, lab2rgb, deltaE } = require("rgb-lab");

import { Box, ColorHSL, ColorRGB, Position } from "./interfaces";

export class Utils {
  private static _colorsCache: Map<ColorRGB, ColorRGB> = new Map<ColorRGB, ColorRGB>();

  static setCSS(element: HTMLElement, style: any) {
    for (const property in style) {
      (element as any).style[property] = style[property];
    }
  }

  static setProperties(element: HTMLElement, props: any) {
    for (const property in props) {
      (element as any)[property] = props[property];
    }
  }

  static getBox(posA: Position, posB: Position): Box {
    const boxX = posA.x < posB.x ? posA.x : posB.x;
    const boxY = posA.y < posB.y ? posA.y : posB.y;

    const boxWidth = Math.abs(posA.x - posB.x);
    const boxHeight = Math.abs(posA.y - posB.y);

    return {
      x: boxX,
      y: boxY,
      width: boxWidth,
      height: boxHeight,
    };
  }

  static colorDistv1(c1: ColorRGB, c2: ColorRGB) {
    let v = 0;

    v += Math.pow(c1.r - c2.r, 2);
    v += Math.pow(c1.g - c2.g, 2);
    v += Math.pow(c1.b - c2.b, 2);

    return Math.sqrt(v);
  }

  static averageColorv2(list: ColorRGB[]): ColorRGB {
    // console.log("avg color v2", list);

    return {
      r: Math.round(Math.sqrt(list.reduce((prev, curr) => prev + Math.pow(curr.r, 2), 0) / list.length)),
      g: Math.round(Math.sqrt(list.reduce((prev, curr) => prev + Math.pow(curr.g, 2), 0) / list.length)),
      b: Math.round(Math.sqrt(list.reduce((prev, curr) => prev + Math.pow(curr.b, 2), 0) / list.length)),
    };
  }

  static averageColorv1(list: ColorRGB[]): ColorRGB {
    return {
      r: Math.round(list.reduce((prev, curr) => prev + curr.r, 0) / list.length),
      g: Math.round(list.reduce((prev, curr) => prev + curr.g, 0) / list.length),
      b: Math.round(list.reduce((prev, curr) => prev + curr.b, 0) / list.length),
    };
  }

  static closestColorv1(list: ColorRGB[], goal: ColorRGB): ColorRGB {
    const cacheHit = Utils._colorsCache.get(goal);

    if (cacheHit) {
      // console.log("cache hit v1");
      return cacheHit;
    } else {
      // console.log("non cache hit v1");
      // console.time("closestv1");

      const distances = list.map((c) => Utils.colorDistv1(c, goal));

      const minDistance = Math.min(...distances);
      //TODO : 2 colors can have the same distance...

      const minDistIndex = distances.findIndex((d) => d === minDistance);

      const colorRGB = list[minDistIndex];

      Utils._colorsCache.set(goal, colorRGB);

      // console.timeEnd("closestv1");

      return colorRGB;
    }
  }

  static closestColorv2(colorsLab: number[][], goal: ColorRGB): ColorRGB {
    const cacheHit = Utils._colorsCache.get(goal);

    if (cacheHit) {
      // console.log("cache hit v2");
      return cacheHit;
    } else {
      // console.log("non cache hit v2");
      // console.time("closestv2");

      const goalLab: number[] = rgb2lab([goal.r, goal.g, goal.b]);

      const distances = colorsLab.map((c) => Utils.colorDistv2(c, goalLab));
      const minDistance = Math.min(...distances);
      //TODO : 2 colors can have the same distance...

      const minDistIndex = distances.findIndex((d) => d === minDistance);

      const color = lab2rgb(colorsLab[minDistIndex]);

      const colorRGB = { r: color[0], g: color[1], b: color[2] };

      Utils._colorsCache.set(goal, colorRGB);

      // console.timeEnd("closestv2");

      return colorRGB;
    }
  }

  static colorDistv2(c1Lab: number[], c2Lab: number[]): number {
    return deltaE(c1Lab, c2Lab);
  }

  static colorsEquals(c1: ColorRGB | undefined, c2: ColorRGB | undefined) {
    return c1 && c2 && c1.r === c2.r && c1.g === c2.g && c1.b === c2.b;
  }

  static sleep(duration: number) {
    return new Promise((res) => setTimeout(res, duration));
  }

  static rgbToHex(r: any, g: any, b: any) {
    r = r.toString(16);
    g = g.toString(16);
    b = b.toString(16);

    if (r.length == 1) r = "0" + r;
    if (g.length == 1) g = "0" + g;
    if (b.length == 1) b = "0" + b;

    return "#" + r + g + b;
  }

  static observeElement(element: HTMLElement, cb: () => void) {
    if (window.MutationObserver) {
      const mutationObserver = new window.MutationObserver(cb);

      mutationObserver.observe(element, { childList: true, subtree: true });
    } else if (window.addEventListener !== undefined) {
      element.addEventListener("DOMNodeInserted", cb);
      element.addEventListener("DOMNodeRemoved", cb);
    }
  }

  /**
   * Converts an RGB color value to HSL. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes r, g, and b are contained in the set [0, 255] and
   * returns h, s, and l in the set [0, 1].
   *
   * @param   Number  r       The red color value
   * @param   Number  g       The green color value
   * @param   Number  b       The blue color value
   * @return  Array           The HSL representation
   */
  static rgbToHsl(color: ColorRGB): ColorHSL {
    let { r, g, b } = color;

    (r /= 255), (g /= 255), (b /= 255);

    let max = Math.max(r, g, b),
      min = Math.min(r, g, b);
    let h: number = 0;
    let s: number = 0;
    let l: number = (max + min) / 2;

    if (max == min) {
      h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

      switch (max) {
        case r:
          h = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g:
          h = (b - r) / d + 2;
          break;
        case b:
          h = (r - g) / d + 4;
          break;
      }

      h /= 6;
    }

    return { h, s, l };
  }
}
