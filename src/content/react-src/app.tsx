import React from "react";
import ReactTooltip from "react-tooltip";
import { start } from "repl";
import { DrawingUtils } from "../../drawingUtils";

import {
  AppState,
  Box,
  Draw,
  ColorRGB,
  Position,
  Multisite,
  SupportedSites,
  Colors,
  DrawingQualities,
  DrawType,
} from "../../interfaces";
import { Utils } from "../../utils";

import "./app.scss";
import { Controller } from "./controller";

interface Props {}

interface State {
  appState: {
    state: AppState;
    draws: Draw[];
    drawIndex: number;
    startedTime: number;
    endedTime: number;
    estimatedFinishTime: number;
  };
  drawingContainer?: HTMLElement;
  firstClick?: Position;
  targetBox?: Box;
  imgUrl?: string;
  drawingQuality?: DrawingQualities;
  colors?: Colors;
}

const gameRootElement = document.querySelector(Multisite.getRootGameElementTag());

//@ts-ignore
const nativeInputValueSetter: (v: any) => void = Object.getOwnPropertyDescriptor(
  window.HTMLInputElement.prototype,
  "value"
).set;

const cancelMouseMove = (e: MouseEvent) => {
  console.log(e.buttons);

  if (e.buttons !== 5) {
    e.stopPropagation();
    e.preventDefault();
  }
  // if (e.isTrusted) {

  // }
};

export class App extends React.Component<Props, State> {
  /**
   *
   */
  constructor(props: Props) {
    super(props);

    this.state = {
      appState: {
        state: AppState.NotInGame,
        draws: [],
        drawIndex: 0,
        startedTime: 0,
        endedTime: 0,
        estimatedFinishTime: 0,
      },
    };

    this.resetState = this.resetState.bind(this);
    this.startDraw = this.startDraw.bind(this);
    this.cancelStartDraw = this.cancelStartDraw.bind(this);
    this.processBackgroundMessage = this.processBackgroundMessage.bind(this);
    this.processNextDraws = this.processNextDraws.bind(this);
    this.pickColor = this.pickColor.bind(this);
    this.pickGarticTool = this.pickGarticTool.bind(this);
    this.pickLolsketchTool = this.pickLolsketchTool.bind(this);
    this.pickQuality = this.pickQuality.bind(this);
    this.drawBox = this.drawBox.bind(this);
    this.draw = this.draw.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onDrawingContainerClick = this.onDrawingContainerClick.bind(this);
    this.listenUserMovements = this.listenUserMovements.bind(this);
    this.ignoreUserMovements = this.ignoreUserMovements.bind(this);
  }

  componentDidMount() {
    Utils.observeElement(gameRootElement as HTMLElement, () => {
      const drawingContainer = document.querySelector(Multisite.getDrawingContainerTag()) as HTMLDivElement;

      if (drawingContainer !== null) {
        this.setState({ drawingContainer, appState: { ...this.state.appState, state: AppState.Waiting } });
      } else {
        this.setState({
          drawingContainer: undefined,
          appState: {
            draws: [],
            drawIndex: 0,
            startedTime: 0,
            endedTime: 0,
            state: AppState.NotInGame,
            estimatedFinishTime: 0,
          },
        });
      }
    });

    chrome.runtime.onMessage.addListener(this.processBackgroundMessage);

    document.addEventListener("keydown", this.onKeyDown);
  }

  resetState() {
    this.setState({ appState: { ...this.state.appState, state: AppState.Waiting } });
  }

  onDrawingContainerClick(e: MouseEvent) {
    const { firstClick } = this.state;

    if (firstClick === undefined) {
      this.setState({ firstClick: { x: e.clientX, y: e.clientY } });
    } else {
      const secondClick: Position = { x: e.clientX, y: e.clientY };

      console.log(firstClick, secondClick);

      const targetBox = Utils.getBox(firstClick, secondClick);

      this.state.drawingContainer?.removeEventListener("click", this.onDrawingContainerClick);

      const { imgUrl, drawingQuality, colors } = this.state;

      const iDrawingQuality = Multisite.getQualityNb(drawingQuality!);

      console.log("idr", iDrawingQuality);

      chrome.runtime.sendMessage({
        command: "get-draws",
        imgUrl,
        targetBox,
        drawingQuality: iDrawingQuality,
        colors,
        drawType: DrawType.Box,
      });

      this.setState({
        targetBox,
        firstClick: undefined,
        imgUrl: undefined,
        colors: undefined,
        appState: { ...this.state.appState, state: AppState.Processing },
      });
    }
  }

  startDraw(imgUrl: string, colors: Colors, drawingQuality: DrawingQualities = DrawingQualities.Medium) {
    this.setState({ imgUrl, colors, drawingQuality, appState: { ...this.state.appState, state: AppState.Clicking } });

    console.log(this.state.drawingContainer);

    this.state.drawingContainer?.addEventListener("click", this.onDrawingContainerClick);
  }

  cancelStartDraw() {
    this.setState({
      firstClick: undefined,
      imgUrl: undefined,
      colors: undefined,
      appState: { ...this.state.appState, state: AppState.Waiting },
    });

    this.state.drawingContainer?.removeEventListener("click", this.onDrawingContainerClick);
  }

  onKeyDown(e: KeyboardEvent) {
    const { state } = this.state.appState;
    switch (e.code) {
      case "Space":
        if (state === AppState.Running) {
          this.setState({ appState: { ...this.state.appState, state: AppState.Paused } });
        } else if (state === AppState.Paused) {
          this.setState({ appState: { ...this.state.appState, state: AppState.Running } });
        }
        break;

      case "Escape":
        if (state === AppState.Running || state === AppState.Paused) {
          this.setState({ appState: { ...this.state.appState, state: AppState.Finished, endedTime: Date.now() } });
        }
        break;
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    const { state: prevAppState } = prevState.appState;
    const { state: actualAppState } = this.state.appState;

    if (prevAppState !== actualAppState) {
      if (actualAppState === AppState.Running) {
        this.ignoreUserMovements();

        this.processNextDraws(1);
      } else {
        this.listenUserMovements();
      }
    }
  }

  ignoreUserMovements() {
    document.addEventListener("mousemove", cancelMouseMove, true);
  }

  listenUserMovements() {
    document.removeEventListener("mousemove", cancelMouseMove, true);
  }

  processBackgroundMessage(message: any, sender: chrome.runtime.MessageSender, cb: (res?: any) => void) {
    console.log("received ", message);

    switch (message.command) {
      case "draws":
        const draws = message.draws;

        const estimatedFinishTime = Date.now() + 8.29 * draws.length * 1.025;

        //app state will be set to Running and the drawing will start with processNextBox in componentDidUpdate
        this.setState({
          appState: {
            state: AppState.Running,
            draws: draws,
            drawIndex: 0,
            startedTime: Date.now(),
            endedTime: 0,
            estimatedFinishTime,
          },
        });

        break;
      default:
        console.log("unknow command : ", message);

        break;
    }

    cb(true);
  }

  async processNextDraws(nb: number) {
    const { state, draws, drawIndex } = this.state.appState;
    const { drawingQuality } = this.state;
    if (state === AppState.Paused) {
      console.log("paused, stop processing");
    } else if (state === AppState.Running) {
      const drawsToDraw = draws.slice(drawIndex, drawIndex + nb);

      this.pickQuality(drawingQuality!);

      for (const draw of drawsToDraw) {
        this.pickTool(draw.type);

        this.pickColor(draw.color);

        this.draw(draw);
      }

      await Utils.sleep(0);

      // const box = boxes[drawIndex];

      if (drawsToDraw.length > 0) {
        this.setState({ appState: { ...this.state.appState, drawIndex: drawIndex + nb } });

        await this.processNextDraws(nb);
      } else {
        this.setState({ appState: { ...this.state.appState, state: AppState.Finished, endedTime: Date.now() } });
      }
    } else {
    }
  }

  pickLolsketchQuality(drawingQuality: DrawingQualities) {
    const qualityButtons = document.querySelectorAll(".weights button");

    if (drawingQuality === DrawingQualities.Small) {
      (qualityButtons.item(0) as HTMLButtonElement).click();
    } else if (drawingQuality === DrawingQualities.Medium) {
      (qualityButtons.item(1) as HTMLButtonElement).click();
    } else if (drawingQuality === DrawingQualities.Big) {
      (qualityButtons.item(2) as HTMLButtonElement).click();
    }
  }

  pickQuality(drawingQuality: DrawingQualities) {
    const site = Multisite.getActualSite();

    if (site === SupportedSites.Garticphone) {
    } else if (site === SupportedSites.Lolsketch && drawingQuality !== DrawingQualities.VerySmall) {
      this.pickLolsketchQuality(drawingQuality);
    }
  }

  pickGarticTool(type: string) {
    const toolElement = document.querySelector(`.tool.${type}`) as HTMLDivElement;

    if (toolElement) {
      if (!toolElement.classList.contains("sel")) {
        toolElement.click();
      }
    }
  }

  pickLolsketchTool(type: string) {
    const svgElement = document.querySelector(`button svg[data-icon="${type}"]`);

    if (svgElement) {
      svgElement.parentElement?.click();
    }
  }

  pickTool(drawType: DrawType) {
    const site = Multisite.getActualSite();

    if (site === SupportedSites.Garticphone && drawType === DrawType.Box) {
      this.pickGarticTool("rec");
    } else if (site === SupportedSites.Garticphone && (drawType === DrawType.Line || drawType === DrawType.Point)) {
      this.pickGarticTool("pen");
    } else if (site === SupportedSites.Lolsketch && (drawType === DrawType.Line || drawType === DrawType.Point)) {
      this.pickLolsketchTool("pen");
    } else {
      throw "site/tool combination not supported";
    }
  }

  pickColor(color: ColorRGB) {
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        const colorPickingElement = document.querySelector("input[type=color]") as HTMLInputElement;

        if (colorPickingElement && colorPickingElement.value !== Utils.rgbToHex(color.r, color.g, color.b)) {
          nativeInputValueSetter.call(colorPickingElement, Utils.rgbToHex(color.r, color.g, color.b));

          colorPickingElement.dispatchEvent(new Event("input", { bubbles: true }));
        }
        break;
      case SupportedSites.Lolsketch:
        const colorName = DrawingUtils.getLolsketchColorName(color);

        if (colorName) {
          const colorButton = document.querySelector(`button[style*="background-color: ${colorName};"]`);

          colorButton?.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        }
        break;
    }
  }

  async draw(draw: Draw) {
    const { targetBox, drawingContainer, drawingQuality } = this.state;

    const iDrawingQuality = Multisite.getQualityNb(drawingQuality!);

    let startXPos = draw.startPos.x;
    let startYPos = draw.startPos.y;
    let endXPos = draw.endPos.x;
    let endYPos = draw.endPos.y;

    if (draw.type === DrawType.Box) {
      ///Box mode
      startXPos = (targetBox?.x ?? 0) + draw.startPos.x * iDrawingQuality! - iDrawingQuality! / 2;

      startYPos = (targetBox?.y ?? 0) + draw.startPos.y * iDrawingQuality! - iDrawingQuality! / 2;

      endXPos = (targetBox?.x ?? 0) + draw.endPos.x * iDrawingQuality! + iDrawingQuality! / 2;

      endYPos = (targetBox?.y ?? 0) + draw.endPos.y * iDrawingQuality! + iDrawingQuality! / 2;
    } else if (draw.type === DrawType.Line) {
      startXPos = (targetBox?.x ?? 0) + draw.startPos.x * iDrawingQuality!;

      startYPos = (targetBox?.y ?? 0) + draw.startPos.y * iDrawingQuality!;

      endXPos = (targetBox?.x ?? 0) + draw.endPos.x * iDrawingQuality!;

      endYPos = (targetBox?.y ?? 0) + draw.endPos.y * iDrawingQuality!;
    }

    chrome.runtime.sendMessage({ command: "mouse-down-d", clientX: startXPos, clientY: startYPos });
    chrome.runtime.sendMessage({ command: "mouse-move-d", clientX: endXPos, clientY: endYPos });
    chrome.runtime.sendMessage({ command: "mouse-up-d", clientX: endXPos, clientY: endYPos });
  }

  async drawBox(startXPos: number, startYPos: number, endXPos: number, endYPos: number) {}

  async drawLolsketchBox(
    drawingContainer: HTMLElement,
    startXPos: number,
    startYPos: number,
    endXPos: number,
    endYPos: number,
    iDrawingQuality: number
  ) {
    console.log(startXPos, startYPos, endXPos, endYPos);

    const nbSquare = (endYPos - startYPos) / iDrawingQuality / 2;

    drawingContainer?.dispatchEvent(
      new MouseEvent("mousedown", {
        clientX: startXPos,
        clientY: startYPos,
        bubbles: true,
      })
    );

    for (let i = 0; i <= nbSquare; i++) {
      drawingContainer?.dispatchEvent(
        new MouseEvent("mousemove", {
          clientX: startXPos + i * iDrawingQuality,
          clientY: startYPos + i * iDrawingQuality,
          bubbles: true,
        })
      );

      await Utils.sleep(10);

      drawingContainer?.dispatchEvent(
        new MouseEvent("mousemove", {
          clientX: endXPos - i * iDrawingQuality,
          clientY: startYPos + i * iDrawingQuality,
          bubbles: true,
        })
      );
      await Utils.sleep(10);

      drawingContainer?.dispatchEvent(
        new MouseEvent("mousemove", {
          clientX: endXPos - i * iDrawingQuality,
          clientY: endYPos - i * iDrawingQuality,
          bubbles: true,
        })
      );
      await Utils.sleep(10);

      drawingContainer?.dispatchEvent(
        new MouseEvent("mousemove", {
          clientX: startXPos + i * iDrawingQuality,
          clientY: endYPos - i * iDrawingQuality,
          bubbles: true,
        })
      );
      await Utils.sleep(10);

      drawingContainer?.dispatchEvent(
        new MouseEvent("mousemove", {
          clientX: startXPos + i * iDrawingQuality,
          clientY: startYPos + i * iDrawingQuality,
          bubbles: true,
        })
      );
      await Utils.sleep(10);
    }

    drawingContainer?.dispatchEvent(
      new MouseEvent("mouseup", {
        clientX: endXPos,
        clientY: endYPos,
        bubbles: true,
      })
    );
  }

  render() {
    const { state, drawIndex, draws, estimatedFinishTime, startedTime, endedTime } = this.state.appState;

    return (
      <div className="ez-draw-container" id="test_patate" hidden={state === AppState.NotInGame}>
        <Controller
          startedTime={startedTime}
          endedTime={endedTime}
          nbDraws={draws.length}
          nbDrawsDrawn={drawIndex + 1}
          state={state}
          estimatedFinishTime={estimatedFinishTime}
          resetState={this.resetState}
          startDraw={this.startDraw}
          cancelStartDraw={this.cancelStartDraw}
        />
        <ReactTooltip place="left" effect="solid" />
      </div>
    );
  }
}
