import React, { useEffect, useState } from "react";
import ProgressBar from "@ramonak/react-progress-bar";

import "./app.scss";
import { AppState, Colors, DrawingQualities, Multisite, SupportedSites } from "../../interfaces";
import ReactTooltip from "react-tooltip";

interface Props {
  state: AppState;
  nbDraws: number;
  nbDrawsDrawn: number;
  startedTime: number;
  endedTime: number;
  estimatedFinishTime: number;
  startDraw: (imgUrl: string, colors: Colors, drawingQuality: DrawingQualities) => void;
  cancelStartDraw: () => void;
  resetState: () => void;
}

export const Controller: React.FunctionComponent<Props> = (props: Props) => {
  const [imgUrl, setImgUrl] = useState("");
  const [drawingQuality, setDrawingQuality] = useState(DrawingQualities.Medium);
  const [colors, setColors] = useState(Multisite.getDefaultColors());
  const [version, setVersion] = useState("v3");

  useEffect(() => {
    ReactTooltip.rebuild();
  }, [props.state]);

  const onStartDrawingClick = () => {
    console.log(props.state);
    if (props.state === AppState.Clicking) {
      props.cancelStartDraw();
    } else {
      console.log(colors, drawingQuality);

      if (
        imgUrl.length > 0 &&
        Object.values(Colors).includes(colors) &&
        Object.values(DrawingQualities).includes(drawingQuality)
      ) {
        props.startDraw(imgUrl, colors, drawingQuality);
      }
    }
  };

  const renderForm = () => {
    const waitingForUserClicks = props.state === AppState.Clicking;
    const isProcessing = props.state === AppState.Processing;
    return (
      <div className="form-container">
        <input
          type="text"
          value={imgUrl}
          placeholder="Image URL"
          onChange={(e) => setImgUrl(e.target.value)}
          disabled={isProcessing || waitingForUserClicks}
        />
        <div className="infos-container">
          <select
            className="quality"
            data-tip="Drawing quality"
            value={drawingQuality}
            onChange={(e) => setDrawingQuality(parseInt(e.target.value))}
            disabled={isProcessing || waitingForUserClicks}
          >
            {renderDrawingQualityOptions(props.state)}
          </select>

          <select
            className="colors"
            data-tip="Colors to use"
            value={colors}
            onChange={(e) => setColors(parseInt(e.target.value))}
            disabled={isProcessing || waitingForUserClicks}
          >
            {renderColorsOptions(props.state)}
          </select>

          {/* <select
            className="mode"
            data-tip="Drawing mode"
            value={colors}
            onChange={(e) => setVersion(e.target.value)}
            disabled={isProcessing || waitingForUserClicks}
          >
            <option value="v3">Drawes</option>
            <option value="v2">Lines</option>
            <option value="v1">Points</option>
          </select> */}
        </div>
        {waitingForUserClicks && <div>Click on 2 points on the book to start drawing</div>}

        <button onClick={onStartDrawingClick} disabled={isProcessing}>
          {waitingForUserClicks ? "Cancel" : "Start drawing"}
        </button>
        {isProcessing && <div>Processing Image...</div>}
      </div>
    );
  };

  const renderFinished = () => {
    const { startedTime, endedTime } = props;

    return (
      <div className="finished-container">
        <div className="infos">
          <div className="drawes-drawn">Drawn {props.nbDrawsDrawn} drawes</div>
          <div className="time">In {(endedTime - startedTime) / 1000} s</div>
        </div>
        <button onClick={props.resetState}>New drawing</button>
      </div>
    );
  };

  const renderDrawing = () => {
    const { nbDraws, nbDrawsDrawn, state, estimatedFinishTime, startedTime } = props;
    const progressPercent = (nbDrawsDrawn * 100) / nbDraws;
    const timestamp = Date.now();

    const nbDrawesPerS = Math.round(nbDrawsDrawn / ((timestamp - startedTime) / 1000));

    const nbDrawesRemaining = nbDraws - nbDrawsDrawn;

    const timer = nbDrawesRemaining / nbDrawesPerS;

    return (
      <div className="running-container">
        <ProgressBar completed={progressPercent.toFixed(2)} transitionDuration="0s" />
        <div className="state">{state}</div>
        <div className="infos">
          Drawing {nbDrawesRemaining} drawes, {nbDrawesPerS} drawes/s, ~{timer.toFixed(2)}s
        </div>

        <div className="keys">Press ESC to cancel/SPACE to pause</div>
      </div>
    );
  };

  const renderColorsOptions = (state: AppState) => {
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        return (
          <>
            <option value={Colors.Gartic}>Gartic</option>
            <option value={Colors.Bits5}>64</option>
            <option value={Colors.Bits8}>256</option>
            <option value={Colors.Bits12}>4096</option>
            {/* <option value={Colors.Bits15}>32768</option> */}
            <option value={Colors.True}>True</option>
          </>
        );
      case SupportedSites.Lolsketch:
        return <option value={Colors.Lolsketch}>Lolsketch</option>;
    }
  };

  const renderDrawingQualityOptions = (state: AppState) => {
    const waitingForUserClicks = state === AppState.Clicking;
    const isProcessing = state === AppState.Processing;
    switch (Multisite.getActualSite()) {
      case SupportedSites.Garticphone:
        return (
          <>
            <option value={DrawingQualities.VerySmall}>Greater</option>
            <option value={DrawingQualities.Small}>Great</option>
            <option value={DrawingQualities.Medium}>Medium</option>
            <option value={DrawingQualities.Big}>Bad</option>
          </>
        );
      case SupportedSites.Lolsketch:
        return (
          <>
            <option value={DrawingQualities.Small}>Great</option>
            <option value={DrawingQualities.Medium}>Medium</option>
            <option value={DrawingQualities.Big}>Bad</option>
          </>
        );
    }
  };

  switch (props.state) {
    case AppState.Running:
    case AppState.Paused:
      return renderDrawing();
    case AppState.Finished:
      return renderFinished();
    case AppState.Processing:
    case AppState.Clicking:
    case AppState.Waiting:
      return renderForm();
    case AppState.NotInGame:
    default:
      return <></>;
  }
};
