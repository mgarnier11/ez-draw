import React from "react";
import ReactDOM from "react-dom";
import ReactTooltip from "react-tooltip";

import { App } from "./react-src/app";

const body = document.querySelector("body");

console.log("content script loaded");

window.addEventListener("load", () => {
  const element = document.createElement("div");

  chrome.runtime.sendMessage({ command: "attach-debugger" });

  body?.appendChild(element);

  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    element
  );
});
